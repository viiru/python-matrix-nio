python-matrix-nio (0.20.1-3) unstable; urgency=medium

  * Team Upload
  * Remove gbp filter for .git* which removes extraneous files
  * Restore .github/workflows/tests.yml
  * Apply patch to support Conduit

 -- Arto Jantunen <viiru@debian.org>  Fri, 03 Mar 2023 07:15:27 +0200

python-matrix-nio (0.20.1-2) unstable; urgency=medium

  * Enable autopkgtest-pkg-pybuild

 -- Jochen Sprickerhof <jspricke@debian.org>  Sat, 10 Dec 2022 08:08:08 +0100

python-matrix-nio (0.20.1-1) unstable; urgency=medium

  * New upstream version 0.20.1
  * Add Multi-Arch according to MA hinter

 -- Jochen Sprickerhof <jspricke@debian.org>  Tue, 15 Nov 2022 15:10:20 +0100

python-matrix-nio (0.20.0-3) unstable; urgency=medium

  * Source only upload.

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 07 Nov 2022 07:18:59 +0100

python-matrix-nio (0.20.0-2) unstable; urgency=medium

  * Reupload after #1022569.

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 06 Nov 2022 23:13:27 +0100

python-matrix-nio (0.20.0-1) unstable; urgency=medium

  * New upstream version
    - Fixes CVE-2022-39254 (Only accept forwarded room keys from our own
      trusted devices)
  * Rebase patches
  * Update d/copyright
  * Update build-depends
  * Move package description to source package

 -- Jochen Sprickerhof <jspricke@debian.org>  Sun, 02 Oct 2022 11:51:17 +0200

python-matrix-nio (0.19.0-4) unstable; urgency=medium

  * Fx pytest exclude syntax (Closes: #1013720)
  * drop Built-Using from arch-all (according to lintian)
  * Fix capitalization (according to lintian)
  * Bump policy version (no changes)
  * Drop dependencies fulfilled by ${python3:Depends}
  * Adopt package.
    Thanks to Jonas for maintaining it (Closes: #1013373)

 -- Jochen Sprickerhof <jspricke@debian.org>  Mon, 04 Jul 2022 22:31:24 +0200

python-matrix-nio (0.19.0-3) unstable; urgency=medium

  * orphan package: set maintainer to Debian QA Group

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 23 Jun 2022 08:08:43 +0200

python-matrix-nio (0.19.0-2) unstable; urgency=medium

  * build-depend on pybuild-plugin-pyproject
    (not dh-python-pep517);
    closes: bug#1005209, thanks to Stefano Rivera

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 09 Feb 2022 10:46:09 +0100

python-matrix-nio (0.19.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info: update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 06 Feb 2022 11:53:22 +0100

python-matrix-nio (0.18.7-2) unstable; urgency=medium

  * use pep517 build system;
    drop patch 2004 to revive setup.py;
    build-depend on dh-python-pep517 python3-poetry-core
    (not python3-setuptools)
  * tighten lintian overrides

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 14 Dec 2021 11:41:32 +0100

python-matrix-nio (0.18.7-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * update copyright info:
    + use Reference field (not License-Reference);
      tighten lintian overrides
    + update coverage

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 12 Oct 2021 01:36:39 +0200

python-matrix-nio (0.18.6-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.6.0

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 25 Aug 2021 20:23:19 +0200

python-matrix-nio (0.18.2-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * add patch 2005 to use python module m2r
    (not newer replacement m2r2 not yet in Debian)
  * unfuzz patch 2002
  * (build-)depend on python3-aiohttp-socks

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Jun 2021 12:05:23 +0200

python-matrix-nio (0.16.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * declare compliance with Debian Policy 4.5.1
  * use debhelper compatibility level 13 (not 12)
  * unfuzz patches
  * add patch 2004 to revive setup.py
  * handle setup.py file provided in patch

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 25 Jan 2021 02:14:50 +0100

python-matrix-nio (0.15.2-1) unstable; urgency=medium

  [ upstream ]
  * new release

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 02 Nov 2020 19:49:26 +0100

python-matrix-nio (0.15.1-3) unstable; urgency=medium

  * really fix pass module name to autopkgtest

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 12 Sep 2020 11:40:15 +0200

python-matrix-nio (0.15.1-2) unstable; urgency=medium

  * fix pass module name to autopkgtest
  * use debhelper compatibility level 12 (not 13),
    to ease backporting
  * simplify dependencies in source
    by letting pydist resolve most possible;
    suppress pydist warning
    about oddly named package python3-pycryptodome

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 11 Sep 2020 13:54:34 +0200

python-matrix-nio (0.15.1-1) unstable; urgency=medium

  [ upstream ]
  * new release

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 01 Sep 2020 20:02:25 +0200

python-matrix-nio (0.15.0-1) unstable; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * unfuzz patches
  * use debhelper compatibility level 13 (not 12)
  * stop tolerate testsuite failures when targeting experimental
  * declare buildsystem explicitly (not in environment)
  * simplify source script copyright-check
  * copyright: extend coverage
  * extend patch 2001 to cover examples page

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 23 Aug 2020 18:55:33 +0200

python-matrix-nio (0.12.0-1) unstable; urgency=medium

  [ upstream ]
  * new release

  [ Jonas Smedegaard ]
  * Update changelog entry for release 0.10.0-2
    to mention changes to descriptions

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 May 2020 13:18:16 +0200

python-matrix-nio (0.11.2-2) unstable; urgency=medium

  * add patch 1001 to use Python module pycryptodome (not crypto);
    see bug#886291, thanks to David Vo
  * (build-)depend on python3-pycryptodome (not python3-crypto)
  * skip test requiring network

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 20 May 2020 11:06:07 +0200

python-matrix-nio (0.11.2-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * copyright: extend coverage
  * stop depend on python3-attr

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 May 2020 14:36:52 +0200

python-matrix-nio (0.10.0-2) experimental; urgency=medium

  * tighten build-dependency on python3-fake-factory;
    drop obsolete notes about pytest-aiohttp
  * copyright: extend coverage
  * update fields Maintainer Uploader:
    maintained in the Matrix Packaging Team
  * fix export DH_OPTIONS
  * build out of source tree
  * improve short and long descriptions to clarify term matrix;
    closes: bug#959092, thanks to Sam Hartman

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 14 May 2020 13:22:58 +0200

python-matrix-nio (0.10.0-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * unfuzz patch 2001
  * add patch 2003 to enable pytest plugin for aiohttp,
    to avoid packaging tiny one-line package pytest-aiohttp

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 29 Mar 2020 20:32:27 +0200

python-matrix-nio (0.9.0-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * simplify rules: drop checking for nocheck (done by debhelper now)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 21 Mar 2020 13:41:26 +0100

python-matrix-nio (0.7.2-1) experimental; urgency=medium

  [ upstream ]
  * new release(s)

  [ Jonas Smedegaard ]
  * (build-)depend on python3-aiofiles
    and (recent python3 or) python3-dataclasses
  * update debhelper;
    build-depend on debhelper-compat dh-sequence-python3
    dh-sequence-sphinxdoc
    (not debhelper dh-python)
  * declare compliance with Debian Policy 4.5.0
  * add copyright holder, and tidy copyright statements

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 20 Feb 2020 14:12:59 +0100

python-matrix-nio (0.6-3) experimental; urgency=medium

  * improve testsuite coverage;
    build-depend on python3-aioresponses python3-atomicwrites
    python3-future python3-hypothesis python3-pytest-benchmark
    python3-pytest-cov
    (but not python3-pytest-aiohttp missing in Debian)
  * fix Vcs-* fields
  * fix (build-)depend on python3-crypto (not python3-pycryptodome)

 -- Jonas Smedegaard <dr@jones.dk>  Tue, 03 Dec 2019 04:28:41 +0100

python-matrix-nio (0.6-2) experimental; urgency=medium

  * relax to depend unversioned on python3-peewee
    (upstream declared requirement too new for Debian
    and possibly unneeded)
  * fix use package section doc for documentation
  * enable autopgktest testsuite
  * add patch to avoid sphinxdoc plugin for github
    (and simplify build options)

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Nov 2019 01:22:36 +0100

python-matrix-nio (0.6-1) experimental; urgency=low

  * Initial packaging release.
    Closes: bug#943435.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 28 Nov 2019 01:22:42 +0100
